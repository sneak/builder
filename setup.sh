#!/bin/bash
################################################################################
## that's a hell of an act.  what do you call it?  "JAVASCRIPT"
################################################################################
mkdir -p "$NVM_DIR"
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.2/install.sh | bash
echo "export NVM_DIR=$NVM_DIR" >> /etc/profile
echo "source \$NVM_DIR/nvm.sh" >> /etc/profile
source /etc/profile

CD="/usr/local/nvm/.cache/bin/node-v18.12.1-linux-arm64/"
mkdir -p "$NCD"
mv /tmp/node-v18.12.1-linux-arm64.tar.xz "$NCD"
nvm ls-remote
nvm install v18.12.1
nvm use v18.12.1

npm install -g npm@9.2.0
npm install -g yarn
yarn global add prettier
################################################################################
## cleanup
################################################################################
rm -rf /usr/local/nvm/.cache /tmp/*
