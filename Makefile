export DOCKER_BUILDKIT := 1
export PROGRESS_NO_TRUNC := 1
APPNAME := builder
ARCH := $(shell uname -m)
GITREV := $(shell git describe --always --dirty=-dirty)
D := $(HOME)/Documents/_SYSADMIN/docker/sneak/$(APPNAME)
BRANCH := $(shell git branch --show-current)
YYYYMMDD := $(shell date -u +%Y-%m-%d)
RN := sneak/builder
export DOCKER_IMAGE_CACHE_DIR := $(HOME)/Library/Caches/Docker/$(APPNAME)-$(ARCH)

.PHONY: default save build

default: save

clean:
	rm -v $(D)/*.tzst

build:
	docker build \
		-t $(RN):$(GITREV)-$(shell uname -m) \
		-t $(RN):latest-$(shell uname -m) \
		-t $(RN):$(YYYYMMDD)-$(shell uname -m) \
		-t $(RN):$(BRANCH)-$(shell uname -m) \
		--progress plain --build-arg GITREV=$(GITREV) .

save: $(D)/latest.tzst $(D)/$(BRANCH).tzst

$(D)/$(BRANCH).tzst: $(D)/$(GITREV)-$(shell uname -m).tzst
	rm -f $@
	ln -s $(D)/$(GITREV)-$(shell uname -m).tzst $@

$(D)/latest.tzst: $(D)/$(GITREV)-$(shell uname -m).tzst
	rm -f $@
	ln -s $(D)/$(GITREV)-$(shell uname -m).tzst $@

$D/$(GITREV)-$(shell uname -m).tzst: build
	docker image inspect $(RN):$(GITREV)-$(shell uname -m)
	-mkdir -p "$(D)"
	docker save $(RN):$(GITREV)-$(shell uname -m) | pv | zstdmt -19 > $@.tmp
	mv $@.tmp $@

push:
	docker push $(RN):$(GITREV)-$(shell uname -m)
	docker push $(RN):latest-$(shell uname -m)
	docker push $(RN):$(BRANCH)-$(shell uname -m)
	docker push $(RN):$(YYYYMMDD)-$(shell uname -m)

