################################################################################
#2345678911234567892123456789312345678941234567895123456789612345678971234567898
################################################################################
FROM debian:bullseye-20221205
ENV DEBIAN_FRONTEND noninteractive
ENV NVM_DIR /usr/local/nvm
ENV GOROOT /usr/local/go
ENV GOPATH /root/go
ENV PATH $PATH:$GOROOT/bin:$GOPATH/bin:$NVM_DIR/versions/node/v18.12.1/bin

RUN --mount=type=cache,target=/var/cache/apt \
    apt update && apt install -y \
    --install-recommends \
    build-essential \
    curl \
    git \
    jq \
    make \
    pv \
    python3 \
    python3-dev \
    strace \
    unzip \
    vim \
    wget \
    zstd

COPY ./deps/* /tmp/

WORKDIR /tmp

RUN unzip protoc-*-linux-$(uname -m).zip -d /usr/local && protoc --version

RUN rm -rf /usr/local/go && \
    tar -C /usr/local -xzf go*.linux-$(uname -m).tar.gz && \
    echo 'export PATH=$PATH:/usr/local/go/bin' >> /etc/profile

RUN \
    go install -v google.golang.org/protobuf/cmd/protoc-gen-go@v1.28.1 && \
    go install -v github.com/golangci/golangci-lint/cmd/golangci-lint@v1.50.1 && \
    go install -v mvdan.cc/gofumpt@latest && \
    go install -v filippo.io/age/cmd/...@latest && \
    go install -v golang.org/x/tools/cmd/godoc@latest && \
    go install -v golang.org/x/tools/gopls@latest && \
    go install -v github.com/klauspost/asmfmt/cmd/asmfmt@latest && \
    go install -v github.com/go-delve/delve/cmd/dlv@latest && \
    go install -v github.com/kisielk/errcheck@latest && \
    go install -v github.com/davidrjenni/reftools/cmd/fillstruct@master && \
    go install -v github.com/rogpeppe/godef@latest && \
    go install -v golang.org/x/tools/cmd/goimports@master && \
    go install -v github.com/mgechev/revive@latest && \
    go install -v honnef.co/go/tools/cmd/staticcheck@latest && \
    go install -v github.com/fatih/gomodifytags@latest && \
    go install -v golang.org/x/tools/cmd/gorename@master && \
    go install -v github.com/jstemmer/gotags@master && \
    go install -v golang.org/x/tools/cmd/guru@master && \
    go install -v github.com/josharian/impl@master && \
    go install -v honnef.co/go/tools/cmd/keyify@master && \
    go install -v github.com/fatih/motion@latest && \
    go install -v github.com/koron/iferr@master && \
    echo done

COPY ./setup.sh /tmp/
RUN bash /tmp/setup.sh
