# builder

[![Build Status](https://drone.datavi.be/api/badges/sneak/builder/status.svg)](https://drone.datavi.be/sneak/builder)

docker base image for building golang

TBD: javascript/node support (in progress)

TBD: python3 support

TBD: ruby support

# goals

* try to put as much stuff as possible in this base image to minimize
  network usage of child image builds

# license

WTFPL
