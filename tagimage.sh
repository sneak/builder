#!/bin/bash

set -euox pipefail

if [[ "$(uname -m)" != "arm64" ]]; then
    echo "wrong box, run on macOS arm workstation after CI" > /dev/stderr
    exit 1
fi

GITREV="$(git describe --always --dirty=-dirty)"
BRANCH="$(git branch --show-current)"
YYYYMMDD="$(date -u +%Y-%m-%d)"
RN="sneak/builder"

for TAG in $GITREV latest $YYYYMMDD $BRANCH ; do
    # pull down the amd64 images from the CI box
    docker pull $RN:$TAG-x86_64
    docker manifest create \
        $RN:$TAG \
        --amend $RN:$TAG-arm64 \
        --amend $RN:$TAG-x86_64
    docker manifest push $RN:$TAG
done
